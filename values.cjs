
function values(object){
    let result = [];
    for(let values of Object.values(object)){
        result.push(values);
    }
    return result;
}

module.exports = values;