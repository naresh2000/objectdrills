function map_object(object , cb){
    let new_obj = {}
    for (let key in object){
        new_obj[key] = cb(object[key]);
    }
    return new_obj;
}

module.exports = map_object;